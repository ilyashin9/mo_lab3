import math

eps = 0.0000001
h = 0.00001


def func(vector):
    return (vector[1] - vector[0]**2)**2 + (1 - vector[0])**2
    #return (vector[1] - vector[0] ** 2) ** 2 + 100 * (1 - vector[0]) ** 2

def x_num_derivative(vector):
    global h
    return (func([vector[0] + h, vector[1]]) - func([vector[0] - h, vector[1]]))/ (2 * h)


def y_num_derivative(vector):
    global h
    return (func([vector[0], vector[1] + h]) - func([vector[0], vector[1] - h]))/ (2 * h)


def x2_num_derivative(vector):
    global h
    return (x_num_derivative([vector[0] + h, vector[1]]) - x_num_derivative([vector[0] - h, vector[1]]))/ (2 * h)


def y2_num_derivative(vector):
    global h
    return (y_num_derivative([vector[0], vector[1] + h]) - y_num_derivative([vector[0], vector[1] - h]))/ (2 * h)

def xy_num_derivative(vector):
    global h
    return (x_num_derivative([vector[0], vector[1] + h]) - x_num_derivative([vector[0], vector[1] - h]))/ (2 * h)

def x_an_derivative(vector):
    return 4*(vector[0] ** 3 - vector[0] * (vector[1] - 50) - 50)


def y_an_derivative(vector):
    return 2*(vector[1] - vector[0] ** 2)


def x2_an_derivative(vector):
    return 4*(3 * vector[0] ** 2 - vector[1] + 50)


def y2_an_derivative():
    return 2


def xy_an_derivative(vector):
    return -2 * vector[0]


def calculate_determinant(m):
    return m[0][0]*m[1][1] - m[0][1]*m[1][0]


def get_gesse(f):
    m = [[x2_num_derivative(f), xy_num_derivative(f)], [xy_num_derivative(f), y2_num_derivative(f)]]
    det = calculate_determinant(m)
    return [[m[1][1]/det, -m[1][0]/det], [-m[0][1]/det, m[0][0]/det]]


def calculate_len(vector):
    len = vector[0][0] ** 2 + vector[1][0] ** 2
    len = math.sqrt(len)
    return len


def gradient(vector):
    return [[x_num_derivative(vector)], [y_num_derivative(vector)]]


def dot_matrices(m, grad):
    res = [[0 for x in range(1)] for y in range(2)]
    for i in range(len(m)):
        for j in range(len(grad[0])):
            for k in range(len(grad)):
                res[i][j] += m[i][k] * grad[k][j]
    return res

def newton_method(x):
    global eps
    k = 0
    while eps < calculate_len(gradient(x)):
        m = get_gesse(x)
        grad = gradient(x)
        #print(calculate_len(grad))
        res = dot_matrices(m, grad)
        x1 = [x[0] - res[0][0], x[1] - res[1][0]]
        x = x1
        k += 1
    print(k)
    print(calculate_len(gradient(x)))
    return x


x0 = [-4, 2]
x = newton_method(x0)
print(x)
print(func(x))
